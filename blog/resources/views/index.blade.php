<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Index Siswa</title>

    <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>
    
</head>
<body>

    <h3>Data Siswa</h3>
    <a href="/siswa/tambah">Tambah Data Siswa</a>
    <br>

    <table border="1">
        <tr>
            <th>Nama</th>
            <th>Email</th>
            <th>Alamat</th>
        </tr>
        @foreach( $siswa as $s )
        <tr>
            <td>{{ $s->nama }}</td>
            <td>{{ $s->email }}</td>
            <td>{{ $s->alamat }}</td>
            <td>
                <a href="/siswa/edit/{{ $s->id }}">Edit</a>
                |
                <a href="/siswa/hapus/{{ $s->id }}">Hapus</a>
            </td>
        </tr>
        @endforeach
    </table>
    <br>

    Halaman : {{ $siswa->currentPage() }}
    <br>
    Jumlah Data : {{ $siswa->total() }}
    <br>
    Data per Halaman : {{ $siswa->perPage() }}

    {{ $siswa->links() }}

</body>
</html>