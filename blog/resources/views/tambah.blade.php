<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Tambah Data</title>
</head>
<body>

    <a href="/siswa">Kembali</a>
    <br>

    <form action="/siswa/store" method="post">
        {{ csrf_field() }}
        <label for="nama">Nama</label>
        <input type="text" id="nama" name="nama" required="required">
        <br>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" required="required">
        <br>
        <label for="alamat">Alamat</label>
        <input type="text" id="alamat" name="alamat" required="required">
        <br>
        <button type="submit">Simpan</button>
    </form>

</body>
</html>