<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Edit Data</title>
</head>
<body>

    <a href="/siswa">Kembali</a>
    <br>

    @foreach( $siswa as $s )
    <form action="/siswa/update" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $s->id }}">
        <label for="nama">Nama</label>
        <input type="text" id="nama" name="nama" required="required" value="{{ $s->nama }}">
        <br>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" required="required" value="{{ $s->email }}">
        <br>
        <label for="alamat">Alamat</label>
        <input type="text" id="alamat" name="alamat" required="required" value="{{ $s->alamat }}">
        <br>
        <button type="submit">Simpan</button>
    </form>
    @endforeach

</body>
</html>