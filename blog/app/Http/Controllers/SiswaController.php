<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function index()
    {
        // ambil data dari tabel siswa
        $siswa = DB::table('siswa')->paginate(5);
        // kirim data siswa ke view index
        return view('index', ['siswa' => $siswa]);
    }

    public function tambah()
    {
        return view('tambah');
    }

    public function store(Request $request)
    {
        // insert data
        DB::table('siswa')->insert([
            'nama' => $request->nama,
            'email' => $request->email,
            'alamat' => $request->alamat
        ]);

        return redirect('/siswa');
    }

    public function edit($id)
    {
        $siswa = DB::table('siswa')->where('id', $id)->get();
        return view('edit', ['siswa' => $siswa]);
    }

    public function update(Request $request)
    {
        DB::table('siswa')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'alamat' => $request->alamat
        ]);

        return redirect('/siswa');
    }

    public function hapus($id)
    {
        DB::table('siswa')->where('id', $id)->delete();

        return redirect('/siswa');
    }
}
