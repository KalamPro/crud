-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Mar 2019 pada 13.42
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id`, `nama`, `email`, `alamat`) VALUES
(2, 'Ashyiap', 'ashyiap@gmail.com', 'pluto'),
(3, 'kalam one', 'ka@gmail', 'mana'),
(4, 'Dewi', 'dew@gmail.com', 'Ciloa'),
(5, 'Gamani Paiman Saptono', 'bakidin.puspasari@gmail.com', 'Ds. Peta No. 565, Tanjungbalai 42109, Banten'),
(6, 'Puspa Hariyah', 'aurora04@yuliarti.biz', 'Ds. Wahidin Sudirohusodo No. 420, Jambi 88326, SumUt'),
(7, 'Manah Ikin Tarihoran M.M.', 'tmegantara@safitri.co.id', 'Kpg. Bakhita No. 956, Cimahi 27728, KepR'),
(8, 'Eka Malika Maryati', 'setiawan.ellis@sihombing.go.id', 'Ki. Basoka Raya No. 452, Sawahlunto 78368, NTB'),
(9, 'Darmaji Mansur', 'wibowo.warsita@pangestu.tv', 'Jln. Umalas No. 271, Pagar Alam 29122, JaTim'),
(10, 'Teddy Sinaga', 'karen39@gmail.co.id', 'Gg. Jaksa No. 645, Ternate 35198, JaTim'),
(11, 'Cemeti Prabu Marpaung', 'sadriansyah@tamba.co', 'Gg. Bara Tambar No. 532, Langsa 78752, JaBar'),
(12, 'Zahra Rahmi Uyainah S.Kom', 'manah.mandala@yahoo.co.id', 'Jln. Halim No. 938, Serang 55107, NTB'),
(13, 'Jasmin Purnawati', 'simbolon.mumpuni@hidayanto.org', 'Ds. Baan No. 925, Tual 35658, Gorontalo'),
(14, 'Prakosa Habibi S.Gz', 'bahuwirya44@haryanti.or.id', 'Jln. Monginsidi No. 859, Samarinda 75852, SulTeng'),
(15, 'Julia Purwanti S.E.', 'jaeman73@tarihoran.com', 'Ds. B.Agam Dlm No. 919, Sukabumi 75022, JaBar'),
(16, 'Raditya Waluyo S.Gz', 'qoktaviani@gmail.com', 'Psr. Suryo Pranoto No. 711, Sibolga 71178, KalSel'),
(17, 'Maras Irsad Setiawan', 'susanti.widya@yahoo.com', 'Ki. Reksoninten No. 579, Pariaman 56822, Banten'),
(18, 'Asirwada Lurhur Siregar', 'gasti33@rahimah.or.id', 'Dk. Muwardi No. 504, Tanjung Pinang 79919, DIY'),
(19, 'Ade Maria Usamah', 'budiyanto.yuliana@sitompul.net', 'Ki. Pasir Koja No. 254, Metro 44579, Jambi'),
(20, 'Joko Hairyanto Mustofa', 'ywahyuni@gmail.com', 'Ds. Bakaru No. 167, Administrasi Jakarta Pusat 34711, Papua'),
(21, 'Mahmud Siregar', 'siregar.natalia@yahoo.com', 'Gg. Yoga No. 597, Samarinda 15043, SumSel'),
(22, 'Putri Hastuti', 'daryani05@gmail.com', 'Kpg. Tentara Pelajar No. 222, Ambon 36592, Bali'),
(23, 'Karimah Nuraini M.Farm', 'lwulandari@gmail.co.id', 'Kpg. Suryo Pranoto No. 192, Lhokseumawe 76773, JaTeng');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
